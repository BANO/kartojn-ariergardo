extern crate ferrum;
#[macro_use]
extern crate ferrum_router;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate crypto;

use serde_json::ser;


mod models;

use ferrum::{Ferrum, FerrumResult, mime, Request, Response};
use ferrum_router::Router;


fn main() {
    let router = router!(
        get "/" => handler,
        get "/games" => get_games,
    );

    let port = std::env::args().nth(1)
        .or(Some("3000".to_owned()))
        .map(|a|
            a.parse::<u16>()
                .expect("Wrong port number")
        )
        .unwrap();

    println!("Using port {}", port);

    Ferrum::new(router)
        .http(("127.0.0.1", port))
        .expect("Failed to start on specified port");
}

fn handler(_: &mut Request) -> FerrumResult<Response> {
    Ok(Response::new().with_content("{\"a\":[1]}", mime::APPLICATION_JSON))
}

fn get_games(_: &mut Request) -> FerrumResult<Response> {
    let games: Vec<models::Game> = vec![];

    let result = serde_json::to_string(&games).unwrap();
    Ok(Response::new().with_content(result, mime::APPLICATION_JSON))
}

